import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {AdduserComponent} from '../adduser/adduser.component';
import {GenricService} from '../genric.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

export interface Element {
  firstName: string;
  lastName: string;
  Username: string;
  city: string;
  department: string;
}

const ELEMENT_DATA: Element[] = [
  { firstName: 'John', lastName: 'Doe', Username: 'johnd', city: 'Nagpur', department: 'IT'},
  { firstName: 'Mike', lastName: 'Hussey', Username: 'mikeh', city: 'pune', department: 'BSG'},
  { firstName: 'Ricky', lastName: 'Hans', Username: 'rickyh', city: 'banglore', department: 'MN'},
  { firstName: 'Martin', lastName: 'Kos', Username: 'martink', city: 'Delhi', department: 'Finance'},
  { firstName: 'Tom', lastName: 'Paisa', Username: 'tomp', city: 'Mumbai', department: 'TIM'}
];

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit, AfterViewInit {
  displayedColumns = ['firstName', 'lastName', 'Username', 'city', 'department'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  private dataSource: MatTableDataSource<Element>;
  constructor(private genericService: GenricService,
              private modalService: NgbModal) {}
  openclick() {
      // const modalRef = this.modalService.open(ModalComponent);
      const modalRef = this.modalService.open(AdduserComponent);
      modalRef.componentInstance.title = 'About';
    }
    ngOnInit() {
    this.dataSource = new MatTableDataSource(ELEMENT_DATA );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}



