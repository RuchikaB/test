import { TestBed, inject } from '@angular/core/testing';

import { GenricService } from './genric.service';

describe('GenricService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenricService]
    });
  });

  it('should be created', inject([GenricService], (service: GenricService) => {
    expect(service).toBeTruthy();
  }));
});
