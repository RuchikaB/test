import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Injectable()
export class GenricService {
  private datasource: any;

  constructor(@Inject(LOCAL_STORAGE) storage: WebStorageService) { }
  setUserData(value): void {
    localStorage.setItem('userdata' , JSON.stringify(value));
    this.datasource = value;
  }
  getuserData() {
    return JSON.parse(localStorage.getItem('userdata' )) || this.datasource ;
  }

}
