import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule, MatPaginatorModule, MatTableModule, MatToolbarModule} from '@angular/material';
import { AdduserComponent } from './adduser/adduser.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { StorageServiceModule} from 'angular-webstorage-service';
import {GenricService} from './genric.service';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    AdduserComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    appRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatInputModule,
    MatToolbarModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    FormsModule,
    StorageServiceModule,
    NgbModule.forRoot()
  ],
  exports: [ MatToolbarModule, MatInputModule, MatTableModule],
  providers: [GenricService, NgbActiveModal],
  bootstrap: [AppComponent],
  entryComponents: [AdduserComponent]
})
export class AppModule { }
