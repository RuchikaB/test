import {Component, Inject, Input, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})

export class AdduserComponent implements OnInit {
  @Input()id: number;
  myForm: FormGroup;
  fname: string;
  lname: string;
  uname: string;
  city: string;
  department: string;
  private userdata: any;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }
  private createForm() {
    this.myForm = this.formBuilder.group({
      firstname: this.fname,
      lastname: this.lname,
      username: this.uname,
      city: this.city,
      department: this.department
    });
  }
  private submitForm() {
    this.activeModal.close(this.myForm.value);
    console.log(JSON.stringify(this.myForm.value));
    this.userdata = this.myForm.value;
    localStorage.setItem('userData', JSON.stringify(this.userdata));
  }
  ngOnInit() {}
}
